from flask_migrate     import Migrate, MigrateCommand
from flask_script      import Manager, Command
from main              import app
from models.sqlalchemy import db
from models.sqlalchemy import Settings, SettingsEnum, EventType

import os

#app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')
db.init_app(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

class InitData(Command):
    'A command to add intial data'
    def run(self):

        #Event types
        event_fixture_1 = EventType(name='TER', interval_minutes=30, color='android_blue') 
        event_fixture_2 = EventType(name='AVAL', interval_minutes=30, color='android_red') 
        event_fixture_3 = EventType(name='NEGOCIO', interval_minutes=30, color='android_green') 
        
        #Settings
        settings_1  = Settings(key='INTERVAL_MINUTES', value= '30')
        settings_2  = Settings(key='JOB_WEEKDAY', value= '0')
        settings_3  = Settings(key='JOB_WEEKDAY', value= '1')
        settings_4  = Settings(key='JOB_WEEKDAY', value= '2')
        settings_5  = Settings(key='JOB_WEEKDAY', value= '3')
        settings_6  = Settings(key='JOB_WEEKDAY', value= '4')
        settings_7  = Settings(key='HOLIDAY_DATE', value= '12-25')
        settings_8  = Settings(key='HOLIDAY_DATE', value= '01-01')
        settings_9  = Settings(key='JOURNAL_INIT_HOUR', value= '08:00.00')
        settings_10 = Settings(key='JOURNAL_END_HOUR', value= '19:00.00')
        settings_11 = Settings(key='JOURNAL_INIT', value= '4|7:00')
        settings_12 = Settings(key='JOURNAL_END', value= '4|14:00')

        db.session.add(event_fixture_1)
        db.session.add(event_fixture_2)
        db.session.add(event_fixture_3)
        db.session.add(settings_1)
        db.session.add(settings_2)
        db.session.add(settings_3)
        db.session.add(settings_4)
        db.session.add(settings_5)
        db.session.add(settings_6)
        db.session.add(settings_7)
        db.session.add(settings_8)
        db.session.add(settings_9)
        db.session.add(settings_10)
        db.session.add(settings_11)
        db.session.add(settings_12)

        db.session.commit()

manager.add_command("initdata", InitData())

if __name__ == '__main__':
    manager.run()

