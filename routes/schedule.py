from flask                  import Flask, request, Blueprint, jsonify, abort
from flask_cors             import CORS
from modules.schedule       import Schedule
from modules.schedule_setup import Settings

import datetime

schedule_blueprint = Blueprint('schedule', __name__)

@schedule_blueprint.route('/schedule/employee/<int:id>', methods = ['GET'])
def schedule_employee(id):
    '''Gets the available schedule by employee/promoter'''
    for param in ['init', 'end', 'event_type']:   
        if param not in request.args:
            abort(400, 'The param {} is mandatory.'.format(param))
    try:
        #12-03-2010 format expected mm-dd-YY
        init = request.args.get('init')
        end  = request.args.get('end')
        event_type = int(request.args['event_type'])   

        datetime.datetime.strptime(request.args.get('init'), "%m-%d-%Y").date()
        datetime.datetime.strptime(request.args.get('end'), "%m-%d-%Y").date()

        #without_validation param expected
        return jsonify(Schedule.get_available(id, init, end, event_type, params=request.args))

    except Exception as e:
        abort(400, str(e))

@schedule_blueprint.route('/schedule/event/types', methods = ['GET'])
def event_types():
    '''Gets the available event types'''
    return jsonify(Settings.eventtypes)