class AgendaSetting(object):
	def __init__(self, id, key, value):
		self.id    = id
		self.key   = str(key)
		self.value = value

class AgendaEventType(object):
	def __init__(self, id, name, interval_minutes, color):
		self.id    = id
		self.color = color
		self.interval_minutes = interval_minutes
		self.name  = name