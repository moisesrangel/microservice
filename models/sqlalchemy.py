from datetime         import datetime
from flask            import Flask
from flask_migrate    import Migrate, MigrateCommand
from flask_script     import Manager
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy       import Integer, Enum

import enum
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')

db      = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)

class SettingsEnum(enum.Enum):
    INTERVAL_MINUTES     = 1
    JOB_WEEKDAY 	     = 2
    HOLIDAY_DATE 	     = 3
    JOURNAL_INIT_HOUR 	 = 4
    JOURNAL_END_HOUR 	 = 5 
    JOURNAL_INIT         = 6
    JOURNAL_END          = 7 

class Settings(db.Model):
    id    = db.Column(db.Integer, primary_key=True)
    key   = db.Column(Enum(SettingsEnum), nullable=False)
    value = db.Column(db.String(255), nullable=False)

class EventType(db.Model):
    id     = db.Column(db.Integer, primary_key=True)
    name   = db.Column(db.String(255), nullable=False)
    interval_minutes = db.Column(db.Integer, nullable=False)
    color   = db.Column(db.String(255), nullable=False)

class Event(db.Model):
    id         = db.Column(db.Integer,  primary_key=True)
    employee   = db.Column(db.Integer, nullable=False)
    init       = db.Column(db.DateTime, nullable=False)
    end        = db.Column(db.DateTime, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

class EventDetail(db.Model):
    id       = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'), nullable=False)
    key      = db.Column(Enum(SettingsEnum), nullable=False)

if __name__ == '__main__':
    manager.run()