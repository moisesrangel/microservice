# Agenda Engine - Schedule

_Microservicio para obtener los tipos de eventos y los horarios disponibles_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

_Todas la librerias necesarias se enlistan en el archivo requirements_

> Python 3.6, pip
> MySQL

```
apt-get install python-pip
pip3 install -r requirements.txt
```

## Variables de entorno

_Es necesario tener un archivo .env con las siguientes variables_

```
	#El valor puede ser sqlalchemy / raw_mysql según el adaptador que se pretenda usar
	DB_TYPE=sqlalchemy
	#Cadena de conexión en caso de que el adaptador sea sqlalchemy
	SQLALCHEMY_DATABASE_URI=mysql://+++++:+++++@11.111.111.111/database
	#Datos para la conexión en caso de que el adaptador sea taw_mysql
	MYSQL_HOST=11.111.111.111
	MYSQL_DATABASE=database
	MYSQL_USER=******
	MYSQL_PWD=******* 
```

## Despliegue 📦

_Para levantar los datos de configuración iniciales_

```
python3.6 manage.py initdata
```

_Para levantar el entorno de desarrollo_

```
python3.6 manage.py runserver
```

## Estructura de módulos 📦

* **data_adapters**
Adaptadores para la conexión a la base de datos

* **migrations**
Migraciones para controlar los cambios en la base de datos para SQLAlchemy

* **models**
Modelos y entidades de datos, las entidades son en general para estandarizar los datos; los modelos son para SQLAlchemy

* **modules**
Capa de negocio, se procesa todas las reglas de negocio

* **routes**
Aquí residen los controladores y sus rutas

* **test**
Test unitarios (pytest)

* **main.py**
Punto de entrada del proyecto

* **manage.py**
Comandos disponibles:


```
  python3.6 manage.py runserver
  python3.6 manage.py initdata
```