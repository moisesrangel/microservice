from data_adapters import Data

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Settings(metaclass=Singleton):
    pass

class Setup(object):

    @staticmethod
    def create_instance():
        Settings.data       = Data().getAccess().settings_all()
        Settings.eventtypes = Data().getAccess().event_types_all()

        Settings.by_key          = lambda n : list(filter(lambda x: x['key'] == n, Settings.data))
        Settings.values_from_key = lambda n : list(map(lambda x : x['value'], Settings.by_key(n)))
        Settings.ints_from_key   = lambda n : list(map(lambda x : int(x['value']), Settings.by_key(n)))