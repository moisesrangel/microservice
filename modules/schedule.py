from modules.schedule_setup import Settings

import datetime
import functools
import itertools
import pandas as pd

class Schedule(object):
    """Actions to manage employee schedule"""

    @staticmethod
    @functools.lru_cache()
    def get_schedule(employeeid, init, end, event_type):
        #Get init hour from settings
        init_hour = Settings.by_key('SettingsEnum.JOURNAL_INIT_HOUR')
        init_hour = '09:00.00' if len(init_hour) == 0 else init_hour[0]['value']

        #Get end hour from settings
        end_hour  = Settings.by_key('SettingsEnum.JOURNAL_END_HOUR')
        end_hour  = '18:00.00' if len(end_hour) == 0 else end_hour[0]['value']
        
        #Get available holidays from Settings
        holidays  = Settings.values_from_key('SettingsEnum.HOLIDAY_DATE')

        #Get custom schedule by day
        hours_init_day = Settings.values_from_key('SettingsEnum.JOURNAL_INIT')
        hours_end_day  = Settings.values_from_key('SettingsEnum.JOURNAL_END')
       
        special_days        = []
        special_hours_init  = {}
        special_hours_end   = {}

        if len(hours_init_day) > 0:
            special_hours_init = dict(map(lambda x : x.split('|'), hours_init_day)) 
            special_days  = list(map(lambda x : int(x.split('|')[0]) , hours_init_day))
            
        if len(hours_end_day) > 0:
            special_hours_end = dict(map(lambda x : x.split('|'), hours_end_day))

        #Get years from date range
        years = []
        years.append(datetime.datetime.strptime(init, "%m-%d-%Y").date().year)
        years.append(datetime.datetime.strptime(end, "%m-%d-%Y").date().year)

        #Build holiday dates    
        holiday_dates = list(itertools.chain.from_iterable(list(map(lambda x : list(map(lambda n : '{}-{}'.format(x, n) , set(years))), holidays))))
        holiday_dates = list(map(lambda x : datetime.datetime.strptime(x, "%m-%d-%Y").date(), holiday_dates ))

        #Get available days from settings
        week_days = Settings.ints_from_key('SettingsEnum.JOB_WEEKDAY')
         
        #Get available request days
        days_range = list(filter(lambda x : x.weekday() in week_days, pd.date_range(start=init, end=end).tolist()))

        #Get interval minutes from event types
        interval = list(filter(lambda x : x['id'] == event_type, Settings.eventtypes))
        if len(interval) == 0:
            raise ValueError('invalid event_type')
        interval  = interval[0]['interval_minutes']
        
        the_range = []
        for day in days_range:
            if day.date() not in holiday_dates:
                final_init_hour = init_hour if day.weekday() not in special_days else special_hours_init[str(day.weekday())]
                final_end_hour  = end_hour  if day.weekday() not in special_days else special_hours_end[str(day.weekday())]
                the_range = the_range + pd.date_range(start='{} {}'.format(day.date(), final_init_hour), end='{} {}'.format(day.date(), final_end_hour), freq='{}T'.format(interval)).tolist()
        
        return the_range

    @staticmethod
    def filter_unavailable(employeeid, the_range):
        return the_range

    @staticmethod
    def get_available(employeeid, init, end, event_type, **kwargs):
        the_range = Schedule.get_schedule(employeeid, init, end, event_type)
        if 'without_validation' in kwargs:
            return [{'employeeid': employeeid, 'schedule' : the_range}]
            
        return [{'employeeid': employeeid, 'schedule' : Schedule.filter_unavailable(employeeid, the_range)}]