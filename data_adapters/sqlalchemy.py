from .base             import DataFactory
from models.entities   import AgendaSetting, AgendaEventType
from models.sqlalchemy import Settings, EventType

class SqlAlchemyData(DataFactory):
	"""SQLAlchemy data access"""
	def __init__(self):
		super(SqlAlchemyData, self).__init__()

	def settings_all(self):
		items = Settings.query.all()
		return list(map(lambda x : AgendaSetting(x.id, x.key, x.value).__dict__, items))

	def event_types_all(self):
		items = EventType.query.all()
		return list(map(lambda x : AgendaEventType(x.id, x.name, x.interval_minutes, x.color).__dict__, items)) 