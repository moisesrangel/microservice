from .sqlalchemy import SqlAlchemyData
from .raw_mysql import RawData

import os

class Data(object):
	def __init__(self):
		super(Data, self).__init__()
		
	def getAccess(self):
		data_dict = {
			"sqlalchemy" : SqlAlchemyData(),
			"raw_mysql"  : RawData(),
		}

		return data_dict[os.getenv('DB_TYPE')]