from .base             import DataFactory
from models.entities   import AgendaSetting, AgendaEventType

import os
import mysql.connector

class RawData(DataFactory):
	"""SQLAlchemy data access"""
	def __init__(self):
		super(RawData, self).__init__()

	def settings_all(self):
		try:
			query  = "SELECT settings.id,  CONCAT('SettingsEnum.', settings.key) as `key`, settings.value FROM settings;"
			cnx    = mysql.connector.connect(user=os.getenv('MYSQL_USER'), password=os.getenv('MYSQL_PWD'), database=os.getenv('MYSQL_DATABASE'), host=os.getenv('MYSQL_HOST'))
			if cnx.is_connected():
				cursor = cnx.cursor(buffered=True)
				cursor.execute(query)
				items = cursor.fetchall()

				return list(map(lambda x : AgendaSetting(x[0], x[1], x[2]).__dict__, items))

		except Exception as e:
			raise ValueError(e)
		finally:
			if cnx.is_connected():
				cursor.close()
				cnx.close()

	def event_types_all(self):
		try:
			query  = "SELECT id, name, interval_minutes, color FROM event_type;"
			cnx    = mysql.connector.connect(user=os.getenv('MYSQL_USER'), password=os.getenv('MYSQL_PWD'), database=os.getenv('MYSQL_DATABASE'), host=os.getenv('MYSQL_HOST'))
			if cnx.is_connected():
				cursor = cnx.cursor(buffered=True)
				cursor.execute(query)
				items = cursor.fetchall()

				return list(map(lambda x : AgendaEventType(x[0], x[1], x[2], x[3]).__dict__, items))

		except Exception as e:
			raise ValueError(e)
		finally:
			if cnx.is_connected():
				cursor.close()
				cnx.close()
