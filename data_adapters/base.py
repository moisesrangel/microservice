from abc import ABCMeta as ABC,abstractmethod

class DataFactory:
    __metaclass__ = ABC
    
    def __init__(self):
        super(DataFactory,self)

    @abstractmethod
    def event_types_all(self):
        pass

    @abstractmethod
    def settings_all(self):
        pass