from dotenv 			    import load_dotenv
from flask                  import Flask
from flask_cors             import CORS
from modules.schedule_setup import Setup
from routes.schedule        import schedule_blueprint

load_dotenv()

app = Flask(__name__)
app.register_blueprint(schedule_blueprint)
CORS(app)

@app.before_first_request
def setup():
    '''Gets the enviroment vars from settings table'''
    Setup.create_instance()

if __name__ == '__main__':
    app.run(threaded=True,host='0.0.0.0', port=8080, debug=True)